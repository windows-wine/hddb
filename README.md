![](ss_hddb0007.png)

This script/application is intended as a helper for retrieving information about the hardware installed in the system. This would be particularly helpful to retro hobbyists who may have a hard time locating matching drivers for their hardware when installing old Windows® versions (95/98/ME/2000/XP/7).

The ANSI executable is an older version (0.0.0.7) as I currently have no AHK 1.0 installed in WINE to compile the latest. It should work relatively fine with old hardware as the built-in database cannot be updated automatically anymore.

There may be errors running this script/application on a 64-bit system. It has not been tested extensively under such configuration.

**© Drugwash, 2010-2023**
