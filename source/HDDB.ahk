; Hardware & Driver DataBase by Drugwash
; Gathers information on currently installed
; hardware and corresponding drivers
;================================================================
;@Ahk2Exe-Obey bits, = %A_PtrSize% * 8
;@Ahk2Exe-Obey xbit, = "%A_PtrSize%" > 4 ? "x64" : "x86"
;@Ahk2Exe-Obey type, = "%A_IsUnicode%" ? "Unicode" : "ANSI"
;@Ahk2Exe-Obey xtype, = "%A_IsUnicode%" ? "W" : "A"
;@Ahk2Exe-Let version = 0.0.0.10
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\HDDB.ico, 159
;@Ahk2Exe-SetFileVersion %U_version%
;@Ahk2Exe-SetProductVersion 0.0.0.0
;@Ahk2Exe-SetProductName Hardware & Driver DataBase
;@Ahk2Exe-SetInternalName HDDB.ahk
;@Ahk2Exe-SetOrigFilename HDDB.exe
;@Ahk2Exe-SetDescription HDDB retrieves and displays hardware and driver info
;@Ahk2Exe-SetCompanyName Drugwash Hobby Programming
;@Ahk2Exe-SetCopyright (c) Drugwash`, Sept 2010 - Aug 2023
;@Ahk2Exe-SetLegalTrademarks Released under the terms of the GNU General Public License
;@Ahk2Exe-Set Comments, Compiled in AHK %A_AhkVersion% %U_type% %U_bits%bit.
;@Ahk2Exe-SetMainIcon %A_ScriptDir%\HDDB.ico
;@Ahk2Exe-UpdateManifest 0, Drugwash.HobbyProgramming.HDDB, %U_version%
;================================================================
SetWorkingDir, %A_Temp%
#NoEnv
#SingleInstance Force
ListLines, Off
SetBatchLines -1
SetControlDelay, -1
;*****************************************
appname = HDDB						; application name
version = 0.0.0.10					; version number
release = August 17, 2023			; release date
type = internal						; release type (internal / public)
iconlocal = %A_ScriptDir%\HDDB.ico	; external icon for uncompiled script
debug = 1							; debug switch (1 = active)
; *****************************************
mail := "drugwash" Chr(64) "mail" Chr(46) "com"
link0 := "http://www" Chr(46) "autohotkey" Chr(46) "com"
abtwin = About %appname%
;DllCall("ole32\CoInitialize", "UInt", 0)
hwpath := A_OSType = "WIN32_NT" ? "System\CurrentControlSet\Enum" : "Enum"
clpath := "HKEY_LOCAL_MACHINE\System\CurrentControlSet\" (A_OSType = "WIN32_NT" ? "Control" : "Services") "\Class"
ispath := "Software\Microsoft\Windows\CurrentVersion\Internet Settings"
keys =
	(LTrim Join
	AutoInsertNotification,BIOSDate,BIOSVersion,BusType,Capabilities,Class
	,ClassGUID,CompatibleIDs,ConfigFlags,ConvMem,CPU,CurrentDriveLetterAssignment
	,DetFlags,DetFunc,DeviceDesc,DeviceType,Disconnect,DMACurrentlyUsed,DPMS
	,Driver,EDID,ExtMem,FailReason,FailReasonID,FailReasonString,FriendlyName
	,HardwareID,HWRevision,IDEMaster,IDENoSerialize,idPrinterPNP,InitAdapterPowerState
	,InitMonitorPowerState,InfName,Int13,MachineType,Manufacturer,MasterCopy
	,MaxResolution,Mfg,Model,MonitorRanges,NoSetupUI,OEMID,PCIDualIDE,PModeInt13
	,PortName,ProductID,Removable,Revision,RevisionLevel,RSDPTRAddr,RSDTAddr,SCSILUN
	,SCSITargetID,Serial,Submodel,SymbolicName,SyncDataXfer,SysfStatus
	,UserDriveLetterAssignment,upperfilters,VerifyKey
	)
;************* Database update *************
; this piece of script is borrowed from VDID info
URL := "http://pci-ids.ucw.cz/v2.2/pci.ids"
URLu := "http://www.linux-usb.org/usb.ids"
msgbox, 0x2024, HDDB update, Update PCI database from the web?
IfMsgBox, No
	{
	gosub prepDB
	goto noupd
	}
RegRead, IsOffLine, HKCU, %ispath%, GlobalUserOffline
if IsOffline
	RegWrite, REG_DWORD, HKCU, %ispath%, GlobalUserOffline, 0
UrlDownloadToFile, %URL%, %A_ScriptDir%\pci1.ids
UrlDownloadToFile, %URLu%, %A_ScriptDir%\usb1.ids
if IsOffline
	RegWrite, REG_DWORD, HKCU, %ispath%, GlobalUserOffline, 1
if (!FileExist(A_ScriptDir "\pci1.ids") || !FileExist(A_ScriptDir "\usb1.ids"))
	{
	MsgBox, 0x1030, %appname% - Information,
		(LTrim
		Cannot download the vendor database!
		Please make sure the application is allowed to
		connect to the Internet and try again.

		The application will now use a built-in DB
		which may not be up-to-date.
		)
	gosub prepDB
	}
else
	{
	FileDelete, %A_ScriptDir%\pci.ids
	FileMove, %A_ScriptDir%\pci1.ids, %A_ScriptDir%\pci.ids
	FileDelete, %A_ScriptDir%\usb.ids
	FileMove, %A_ScriptDir%\usb1.ids, %A_ScriptDir%\usb.ids
	FileCopy, %A_ScriptDir%\pci.ids, %A_WorkingDir%
	FileCopy, %A_ScriptDir%\usb.ids, %A_WorkingDir%
	}
noupd:
vdb := A_WorkingDir "\pci.ids"
udb := A_WorkingDir "\usb.ids"
if !FileExist(vdb) || !FileExist(udb)
	msgbox, Error in database
;************* Custom tray menu *************
if ! notray
	Menu, Tray, Icon, % A_IsCompiled ? "" : iconlocal
Menu, Tray, NoStandard
Menu, Tray, Add, Settings, options
Menu, Tray, Default, Settings
Menu, Tray, Add, About, about
Menu, Tray, Add
Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, getout

Menu, LB, Add, Copy name, lbcopy
Menu, LB, Add, Open in RegEdit, lbopen
Menu, LB, Add
Menu, LB, Add, Save list, lbsave


Menu, LVsave, Add, CSV, lvsave1
Menu, LVsave, Add, ini, lvsave2
Menu, LVsave, Add, HTML, lvsave3
Menu, LVsave, Add, text, lvsave4
Menu, LV, Add, Copy value, lvcopy
Menu, LV, Add, Copy key && value, lvcopyall
Menu, LV, Add
Menu, LV, Add, Open in RegEdit, lvopen
Menu, LV, Add
Menu, LV, Add, Save list as, :LVsave
Menu, LB, Disable, Open in RegEdit
Menu, LV, Disable, Open in RegEdit
Menu, LVsave, Disable, HTML
Loop, HKLM, Hardware\Description\System\CentralProcessor\0, 1, 1
	{
	if (A_LoopRegName = "Identifier")
		RegRead, CPUtype
	if (A_LoopRegName = "VendorIdentifier")
		RegRead, CPUvendor
	}
Gui, 1:Add, ListBox, x6 y211 w110 h200 Sort 0x100 -Multi vcat gselcat,
Gui, 1:Add, ListBox, x120 y211 w336 h200 AltSubmit -Sort 0x100 -Multi vsel gselect,
Gui, 1:Add, ListView, x6 y7 w450 h200 vLV, Key name|Value
Gui, 1:Add, Text, x6 y414 w30 h20 0x200 vlbl1 Right, VEN :
Gui, 1:Add, Edit, x36 y414 w40 h20 Limit4 Uppercase vvid,
Gui, 1:Add, Text, x6 y434 w30 h20 0x200 vlbl2 Right, DEV :
Gui, 1:Add, Edit, x36 y434 w40 h20 Limit4 Uppercase vdid,
Gui, 1:Add, Edit, x78 y415 w378 h18 +0x900 -0x4A00080 -E0x300 vinfo2,
Gui, 1:Add, Edit, x78 y435 w378 h18 +0x900 -0x4A00080 -E0x300 vinfo1,
Gui, 1:Add, Text, x78 y459 w32 h16 +0x200 +Disabled Hidden, CPU :
Gui, 1:Add, Text, x110 y459 w134 h16 +0x200 +Disabled,
Gui, 1:Add, Text, x78 y476 w166 h16 +0x200 +Disabled,
Gui, 1:Add, Button, x6 y475 w70 h20, USB
Gui, 1:Add, Button, x6 y455 w70 h20, Generic
Gui, 1:Add, Button, x246 y455 w100 h40, Refresh
Gui, 1:Add, Button, x356 y455 w100 h40, Exit
; Generated using SmartGUI Creator 4.3w
Loop, HKLM, %hwpath%, 2, 0
	GuiControl,, ListBox1, %A_LoopRegName%
GuiControl, 1:Choose, ListBox1, 1
if CPUVendor
	{
	GuiControl, Show, Static3
	GuiControl,, Static4, %CPUVendor%
	GuiControl,, Static5, %CPUType%
	}
Gui, 1:Show, x215 y85 w462 h502, %appname%
Gui,  1:+LastFound
hMain := WinExist()
; *****************************************
selcat:
Gui, Submit, NoHide
idx=0
Loop, HKLM, %hwpath%\%cat%, 1, 1
	if (A_LoopRegName = "DeviceDesc")
		{
		idx++
		RegRead, deviceName_%cat%_%idx%
		Loop, %A_LoopRegKey%, %A_LoopRegSubKey%, 0, 1
			{
			if A_LoopRegType = key
				continue
			RegRead, z
;IfInString, A_LoopRegName, %A_Space%
;	msgbox, Key=%A_LoopRegKey%`nSubkey=%A_LoopRegSubKey%`nName=%A_LoopRegName%
			StringReplace, zz, A_LoopRegName, \, �, All
			StringReplace, zz, zz, `&, �, All
			StringReplace, zz, zz, *, _, All
			StringReplace, zz, zz, %A_Space%, �, All
			a_%cat%_%idx%_%zz% := z
			}
		}
GuiControl,, ListBox2, |
Loop, %idx%
	GuiControl,, ListBox2, % deviceName_%cat%_%A_Index%
LV_Delete()
return

select:
Gui, Submit, NoHide
LV_Delete()
Loop, Parse, keys, CSV
	if a_%cat%_%sel%_%A_LoopField%
		{
		StringReplace, i, A_LoopField, �, \, All
		StringReplace, i, i, �, `&, All
		StringReplace, i, i, �, %A_Space%, All
		StringReplace, j, a_%cat%_%sel%_%A_LoopField%, �, \, All
		StringReplace, j, j, �, `&, All
		StringReplace, j, j, �, %A_Space%, All
		StringReplace, j, j, `n, `,, All
		LV_Add("", i, j)
		}
Loop, 2
	LV_ModifyCol(A_Index, "AutoHdr")
i := a_%cat%_%sel%_HardwareID
if (!InStr(i, "\VEN_") && !InStr(i, "\VID_"))
	i := a_%cat%_%sel%_CompatibleIDs
if InStr(i, "\VEN_")
	{
	GuiControl,, vid, % SubStr(i, InStr(i, "VEN_")+4, 4)
	GuiControl,, did, % SubStr(i, InStr(i, "DEV_")+4, 4)
	goto ButtonGeneric
	}
else if InStr(i, "\VID_")
	{
	GuiControl,, vid, % SubStr(i, InStr(i, "VID_")+4, 4)
	GuiControl,, did, % SubStr(i, InStr(i, "PID_")+4, 4)
	goto ButtonUSB
	}
else
	{
	GuiControl,, vid,
	GuiControl,, did,
	GuiControl,, info1,
	GuiControl,, info2,
	return
	}

ButtonGeneric:
Gui, Submit, NoHide
if StrLen(vid did) = 8
	{
	GuiControl,, lbl1, VEN :
	GuiControl,, lbl2, DEV :
	GuiControl,, info1,
	GuiControl,, info2,
	v3 := GetVendorStr(vid, v5 := did, vdb)
	GuiControl,, info1, %v5%
	GuiControl,, info2, %v3%
	Gui, Submit, NoHide
	if !info1
		GuiControl,, info1, <Unknown>
	}
return

ButtonUSB:
Gui, Submit, NoHide
if StrLen(vid did) = 8
	{
	GuiControl,, lbl1, VID :
	GuiControl,, lbl2, PID :
	GuiControl,, info1,
	GuiControl,, info2,
	v3 := GetVendorStr(vid, v5 := did, udb, 1)
	GuiControl,, info1, %v5%
	GuiControl,, info2, %v3%
	Gui, Submit, NoHide
	if !info1
		GuiControl,, info1, <Unknown>
	}
return

ButtonRefresh:
GuiControl,, ListBox1, |
Loop, HKLM, %hwpath%, 2, 0
	GuiControl,, ListBox1, %A_LoopRegName%
goto selcat
return

GuiContextMenu:
;msgbox, A_EventInfo=%A_EventInfo%, A_GuiControl=%A_GuiControl%
if !A_EventInfo
	return
row := A_EventInfo
if A_GuiControl = sel
	{
	row := LBfix("ListBox2")
	GuiControl, 1:Choose, ListBox2, %row%
	Gui, Submit, NoHide
	Menu, LB, Show
	}
else if A_GuiControl = LV
	Menu, LV, Show
return

lbcopy:
ControlGet, i, List,, ListBox2, A
Loop, Parse, i, `n
	if (A_Index = sel)
	{
	Clipboard := A_LoopField
	break
	}
return

lbopen:
SendMessage, 0x18A, row-1, 0, ListBox2, A	; LB_GETTEXTLEN
aloc := VarSetCapacity(buf, ErrorLevel+1, 0)
SendMessage, 0x189, row-1, &buf, ListBox2, A	; LB_GETTEXT
; Error may appear when ANSI & Unicode text is mixed:
; string lenght returned above may be shorter.
; We should use the value returned in ErrorLevel by LB_GETTEXT
; to adjust buffer size, if needed
if (ErrorLevel > aloc)
	{
	VarSetCapacity(buf, ErrorLevel+1, 0)
	SendMessage, 0x189, row-1, &buf, ListBox2, A	; LB_GETTEXT
	}
VarSetCapacity(buf, -1)
regpath := "HKEY_LOCAL_MACHINE\" hwpath "\" cat "\" buf
;msgbox, Open RegEdit to %regpath%
wcls = RegEdit_RegEdit
Run, RegEdit
WinActivate, ahk_class %wcls%
WinWaitActive, ahk_class %wcls%
ControlGet, hRETV, Hwnd,, SysTreeView321, ahk_class %wcls%
rID := DllCall("SendMessage"
		, "UInt", hRETV
		, "UInt", 0x110A	; TVM_GETNEXTITEM
		, "UInt", 0			; TVGN_ROOT
		, "UInt", 0)
msgbox, RegEdit TV handle: %hRETV%`nrootID: %rID%
Loop, Parse, regpath, \
	{
	pID := A_Index=1 ? rID : nID
	cText := A_LoopField
	Loop
		{
		if A_Index=1
			nID := DllCall("SendMessage"
				, "UInt", hRETV
				, "UInt", 0x110A	; TVM_GETNEXTITEM
				, "UInt", 0x4		; TVGN_CHILD
				, "UInt", pID)
		else
			nID := DllCall("SendMessage"
				, "UInt", hRETV
				, "UInt", 0x110A	; TVM_GETNEXTITEM
				, "UInt", 0x1		; TVGN_NEXT
				, "UInt", nID)
		if !nID
			break
		VarSetCapacity(strg, 256, 32)
		VarSetCapacity(TVITEM, 40, 0)
;		mask := 0x40 | 0x2 | 0x20 | 0x8 | 0x1 | 0x4 ;
		mask := 0x10 | 0x1 ; TVIF_HANDLE, TVIF_TEXT
		NumPut(mask, TVITEM, 0, "UInt")
		NumPut(nID, TVITEM, 4, "UInt")
		NumPut(&strg, TVITEM, 16, "UInt")
		NumPut(255, TVITEM, 20, "Int")
;msgbox, (1) index=%A_Index%`ncText=%cText%`nstrg=%strg%`nitemID=%nID%
		r := DllCall("SendMessage"
			, "UInt", hRETV
			, "UInt", 0x110C	; TVM_GETITEMA
			, "UInt", 0
			, "UInt", &TVITEM)
		strg = %strg%
msgbox, (2) return value: %r%`nindex=%A_Index%`ncText=%cText%`nstrg=%strg%`nitemID=%nID%
		if (cText = strg)
			{
			SendMessage, 0x1102, 0x2, nID,, ahk_id %hRETV%
			DllCall("SendMessage"
				, "UInt", hRETV
				, "UInt", 0x1102	; TVM_EXPAND
				, "UInt", 0x2		; TVE_EXPAND
				, "UInt", nID)
			break
			}
		}

	}
return

lbsave:
ControlGet, i, List,, ListBox2, A
FileSelectFile, filename, S10, %A_ScriptDir%\%cat%_full_list.txt
	, Select filename and location to save:, Documents (*.txt)
if filename
	FileAppend, %i%, %filename%
return

lvcopy:
LV_GetText(i, row, 2)
Clipboard := i
return

lvcopyall:
LV_GetText(i, row, 1)
LV_GetText(j, row, 2)
Clipboard := i A_Tab j
return

lvopen:
lvsave1:
ControlGet, i, List,, SysListView321, A
StringReplace, i, i, %A_Tab%, `,, All
goto lvsavegen
lvsave2:
ControlGet, i, List,, SysListView321, A
StringReplace, i, i, %A_Tab%, `=, All
ControlGet, j, List,, ListBox2, A
Loop, Parse, j, `n
	if (A_Index = sel)
	{
	i := "[" A_LoopField "]`n" i
	break
	}
goto lvsavegen
lvsave3:
ControlGet, i, List,, SysListView321, A
lvsave4:
ControlGet, i, List,, SysListView321, A
lvsavegen:
FileSelectFile, filename, S10, %A_ScriptDir%\%cat%_device%sel%_list.txt
	, Select filename and location to save:, Documents (*.txt)
if filename
	FileAppend, %i%, %filename%
return

prepDB:
ifnotexist %A_ScriptDir%\pci.ids
	FileInstall, pci.ids, %A_WorkingDir%\pci.ids
else
	FileCopy, %A_ScriptDir%\pci.ids, %A_WorkingDir%
ifnotexist %A_ScriptDir%\usb.ids
	FileInstall, usb.ids, %A_WorkingDir%\usb.ids
else
	FileCopy, %A_ScriptDir%\usb.ids, %A_WorkingDir%
ifexist %A_ScriptDir%\pci1.ids
	FileDelete, %A_ScriptDir%\pci1.ids
ifexist %A_ScriptDir%\usb1.ids
	FileDelete, %A_ScriptDir%\usb1.ids
return

options:
return

reload:
Reload

getout:
ButtonExit:
GuiEscape:
GuiClose:
FileDelete, %A_WorkingDir%\pci.ids
FileDelete, %A_WorkingDir%\usb.ids
FileDelete, %A_ScriptDir%\VendorID_DB.txt
FileDelete, %A_ScriptDir%\USB_devices.txt
;DllCall("ole32\CoUninitialize")
ExitApp
;************** ABOUT box *****************
about:
DetectHiddenWindows, Off
wState := WinExist(appname)
;DetectHiddenWindows, On
Gui 1:+Disabled
if wState
	Gui, 1:Hide
Gui, 2:+Owner1 -MinimizeBox
if ! A_IsCompiled
	Gui, 2:Add, Picture, x69 y10 w32 h-1, %iconlocal%
else
	Gui, 2:Add, Picture, x69 y10 w32 h-1 Icon1, %A_ScriptName%
Gui, 2:Font, Bold
Gui, 2:Add, Text, x5 y+10 w160 Center, %appname% v%version%
Gui, 2:Font
Gui, 2:Add, Text, xp y+2 wp Center, by Drugwash
Gui, 2:Add, Text, xp y+2 wp Center gmail0 cBlue, %mail%
Gui, 2:Add, Text, xp y+10 wp Center, Released %release%
Gui, 2:Add, Text, xp y+2 wp Center, (%type% version)
Gui, 2:Add, Text, xp y+10 wp Center, This product is open-source,
Gui, 2:Add, Text, xp y+2 wp Center, developed in AutoHotkey
Gui, 2:Font,CBlue Underline
Gui, 2:Add, Text, xp y+2 wp Center glink0, %link0%
Gui, 2:Font
if iconpack
	{
	Gui, 2:Add, Text, xp y+5 wp Center, Icon from %iconpack% at
	Gui, 2:Font,CBlue Underline
	Gui, 2:Add, Text, xp y+2 wp Center glink1, %link1%
	Gui, 2:Font
	}
Gui, 2:Add, Button, xp+40 y+20 w80 gdismiss Default, &OK
Gui, 2:Show,, %abtwin%

if ! hCurs
	{
	hCurs := DllCall("LoadCursor", "UInt", NULL, "Int", 32649, "UInt") ;IDC_HAND
	OnMessage(0x200, "WM_MOUSEMOVE")
	}
return

dismiss:
Gui, 1:-Disabled
Gui, 2:Destroy
if wState
	Gui, 1:Show
IfWinNotExist, %appname%
	{
	OnMessage(0x200,"")
	DllCall("DestroyCursor", "Uint", hCurs)
	}
return

mail0:
mail0 = mailto:%mail%
link0:
link1:
Run, %A_ThisLabel%,, UseErrorLevel
return

WM_MOUSEMOVE(wParam, lParam)
{
Global hCurs, abtwin, appname
WinGetTitle, wind
MouseGetPos,,, winid, ctrl
; change StaticX below to suit the actual window(s)
if wind in %abtwin%
	if ctrl in Static4,Static9,Static11
		DllCall("SetCursor", "UInt", hCurs)
return
}
;================================================================
LBfix(ctrl, gui="1")
;================================================================
{
CoordMode, Mouse, Relative
MouseGetPos, mx, my
GuiControlGet, h%ctrl%, %gui%:Hwnd, %ctrl%
ControlGetPos, cx, cy,,,, % "ahk_id " h%ctrl%
ax := mx-cx
ay := my-cy
lP := (ay & 0xFFFF) << 16 | (ax & 0xFFFF)
SendMessage, 0x1A9, 0, lP, %ctrl%, A	; LB_ITEMFROMPOINT
item := 1 + (ErrorLevel & 0xFFFF)
;GuiControl, %gui%:Choose, %ctrl%, %item%
return item
}
;================================================================
GetVendorStr(vid, ByRef did, dbfile, usb=0)
;================================================================
{
Static vdb, udb
if (!vdb && !usb)
	FileRead, vdb, %dbfile%
else if (!udb && usb)
	FileRead, udb, %dbfile%
RegExMatch(usb ? udb : vdb, "imsU)(*ANYCRLF)^" vid ".*(?=(\n\d|\n\n))", s1)
RegExMatch(s1, "imU)^" vid "\s+?\K.*(?=(\n|$))", v1)
if v1
	{
	RegExMatch(s1, "imsU)(*ANYCRLF)^\t\K" did ".*(?=$)", s2)
;	RegExMatch(s2, "imU)^\t" did "\s+?\K.*(?=(\n|$))", v2)
	RegExMatch(s2, "imU)^" did "\s+?\K.*(?=(\n|$))", v2)
	did := v2 ? v2 : did
	return v1
	}
return "<ukn>"
}
